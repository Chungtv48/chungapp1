//
//  AppDelegate.swift
//  FlurryBlankApp
//
//  Created by Phu Pham Cong on 1/15/18.
//  Copyright © 2018 Phu Pham Cong. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let AMZ_URL = "https://amz.firebaseio.com/app4.json"
    static let AMZ_AD_DICT = "AmazonAdsDict"
    static let EXPIRED_DATE = "ExpiredDate"
    let defaults = UserDefaults.standard
    var window: UIWindow?
    static var AMZ_AD_KEY = ""
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        defaults.set("1", forKey: "IABConsent_SubjectToGDPR")
        defaults.set("1", forKey: "aps_gdpr_pub_pref_li")
        if checkExpiredDate() {
            let amzUrl = URL(string: AMZ_URL)
            do {
                let amzData = try Data(contentsOf: amzUrl!)
                if let amzDict = try JSONSerialization.jsonObject(with: amzData) as? NSDictionary {
                    self.defaults.set(amzDict, forKey: AppDelegate.AMZ_AD_DICT)
                    UserDefaults.standard.set(Date(timeIntervalSinceNow: 3600.0), forKey: AppDelegate.EXPIRED_DATE)
                }
            } catch {
            }
        }
        
        if let apiAmzDict = UserDefaults.standard.dictionary(forKey: AppDelegate.AMZ_AD_DICT)
        {
            let randNum = arc4random_uniform(UInt32(apiAmzDict.count))
            let amzApiKey = Array(apiAmzDict.values)[Int(randNum)] as! String
            AppDelegate.AMZ_AD_KEY = amzApiKey
            AmazonAdRegistration.shared().setAppKey(amzApiKey)
            AmazonAdRegistration.shared().setLogging(false)
        }
        return true
    }
    
    func checkExpiredDate() -> Bool {
        let expiredDate = UserDefaults.standard.object(forKey: AppDelegate.EXPIRED_DATE) as? Date
        if expiredDate == nil {
            return true
        } else {
            let currentDate = Date()
            if currentDate > expiredDate! {
                return true
            } else {
                return false
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

